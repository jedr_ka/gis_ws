
#!/usr/bin/env python3

import argparse
import sys
from math import sqrt
from random import shuffle
import networkx as nx
from math import factorial, e
from sys import exit
from numpy import random
import collections
import matplotlib.pyplot as plt

# funkcja znajdująca przybliżoną średnią długość najkrótszych ścieżek
# ala Monte Carlo
def fast_average_shortest_path_length(G, delta_lim=0.05, convergence_window=100, debug_aprox=False, debug=False):
    nodes = list(G.nodes)  # wyczytaj listę wierzchołków w grafie
    shuffle(nodes)  # przetasuj losowo listę wierzchołkówtest_results
    s = 0  # suma długości ścieżek
    cnt = 0  # liczba ścieżek
    aspl = 0  # wyliczona średnia długość najkrótszych ścieżek
    old_aspl = []  # lista poprzednich wartości aspl

    for i, node in enumerate(nodes):

        # wyznacz wszystkie najkrótsze ścieżki łączące wierzchołek node z pozostałymi
        paths = nx.single_source_shortest_path_length(G, node)
        # dodaj do zmiennej s sumę długości znalezionych ścieżek
        s = s + sum(paths.values())
        # dodaj do licznika ścieżek liczbę znalezionych ścieżek
        cnt = cnt + len(paths)
        # wyznacz nową średnią
        aspl = s / cnt
        old_aspl.append(aspl)

        if debug and i % 10 == 0:
            print(aspl)

        if i > convergence_window and max(old_aspl[-convergence_window:]) - min(
                old_aspl[-convergence_window:]) < delta_lim:

            if debug:
                g_possible_paths = len(G.nodes) ** 2
                print("converged after analyzing {:.3}% of paths and {} iterations".format(cnt / g_possible_paths * 100,
                                                                                           i))

            if debug_aprox:
                real_aspl = nx.average_shortest_path_length(G)
                print("[aspl_aprox{:.3} aspl_real{:.3} rel err{:.3}%]".format(aspl, real_aspl,
                                                                              (aspl - real_aspl) / real_aspl * 100))

            return aspl

    return aspl


def choose(n, k):
    """
    A fast way to calculate binomial coefficients by Andrew Dalke (contrib).
    """
    if 0 <= k <= n:
        ntok = 1
        ktok = 1
        for t in range(1, min(k, n - k) + 1):
            ntok *= n
            ktok *= t
            n -= 1
        return ntok // ktok
    else:
        return 0


# funkcja gęstości rozkładu stopni wierzchołków w sieci W-S dla danych parametrów
def P(n, k, p, m):
    k = int(round(k))

    def inner(n, k, p, m, x):
        return choose(2 * k, x) * (p / 2) ** x * (1 - (p / 2)) ** (2 * k - x) * (
                (k * p) ** (m - (2 * k) + x)) / factorial((m - (2 * k) + x)) * e ** (-k * p)

    return sum(inner(n, k, p, m, x) for x in range(max(2 * k - m, 0), min(n - 1 - m, 2 * k)))


def degdistrfit(G: nx.Graph, eps=0.00001, debug=False):
    n = len(G)  # rząd grafu
    phi = [degree[1] for degree in G.degree]  # lista stopni wierzchołków grafu
    phi_max = max(phi)
    s = len(phi)  # długość listy phi

    # wyliczanie średniego stopnia wierzchołków w grafie
    K_dash = sum(phi_i / (2 * s) for phi_i in phi)

    if debug:
        print("K_dash {}".format(K_dash))

    # wyznaczanie odchylenia standardowego stopni wierzchołków w grafie
    sigma_dash = sqrt(sum((phi_i - (2 * K_dash)) ** 2 / (s - 1) for phi_i in phi))

    if debug:
        print("sigma_dash {}".format(sigma_dash))

    # zakres przeszukiwania binarnego
    p_l = 0  # początek przeszukiwanego zakresu
    p_u = 1  # koniec przeszukiwanego zakresu
    p_dash = (p_l + p_u) / 2  # bieżący przeszukiwany punkt

    # wyliczenie początkowych wartości

    # średni stopień wierzchołka w sieci W-S(n, K_dash, p_dash)
    micro_tilde = sum(m * P(n, K_dash, p_dash, m) for m in range(min(n, int(phi_max))))
    # odchylenie standardowe stopni wierzchołków w sieci W-S(n, K_dash, p_dash)
    sigma_tilde = sqrt(sum((m - micro_tilde) ** 2 * P(n, K_dash, p_dash, m) for m in range(min(n, int(phi_max)))))

    iter = 1
    while abs(sigma_tilde - sigma_dash) > eps:

        # przeszukiwanie binarne
        # jeśli średni stopień wierzchołka w sieci W-S(n, K_dash, p_dash) jest wiekszy niż w grafie wejściowym
        if sigma_tilde > sigma_dash:
            # poszukiwana warość p_dash leży w dolnej połowie zakresu (p_u, p_d)
            p_u = p_dash
        else:
            # poszukiwana warość p_dash leży w górnej połowie zakresu (p_u, p_d)
            p_l = p_dash

        # aktualizacja sprawdzanego punktu
        p_dash = (p_l + p_u) / 2

        # przeliczanie parametrów
        micro_tilde = sum(m * P(n, K_dash, p_dash, m) for m in range(min(n, 100)))
        sigma_tilde = sqrt(sum((m - micro_tilde) ** 2 * P(n, K_dash, p_dash, m) for m in range(min(n, 100))))

        if debug:
            print("p_dash {} micro_tilde {} sigma_tilde {}".format(p_dash, micro_tilde, sigma_tilde))

        # inkrementacja licznika iteracji
        iter = iter + 1

        if iter % 50 == 0:
            print('Iteration {} of approximation algorithm: sigma diff = {:.3f}'.format(iter, abs(
                sigma_tilde - sigma_dash)))  # usuń mnie

        # zakończenie wykonywania jeśli przekroczono 100 iteracji
        if iter == 100:
            print('Approximation algorithm terminated, maximum iterations reached')
            break

    # zwracanie wyznaczonych parametrów sieci W-S
    return n, int(round(K_dash * 2)), p_dash


def delete_edges(g, n_edges):
    for n in range(n_edges):
        edges = list(g.edges)
        chosen_edge = edges[random.choice(len(edges))]
        g.remove_edge(chosen_edge[0], chosen_edge[1])
    return g


def degree_graph(g):

    degree_sequence = sorted([d for n, d in g.degree()], reverse=True)  # degree sequence
    degree_count = collections.Counter(degree_sequence)
    deg, cnt = zip(*degree_count.items())

    fig, ax = plt.subplots()
    plt.bar(deg, cnt, width=0.80, color='b')

    plt.title("Degree Histogram")
    plt.ylabel("Count")
    plt.xlabel("Degree")
    ax.set_xticks([d + 0.4 for d in deg])
    ax.set_xticklabels(deg)

    # draw graph in inset
    # plt.axes([0.4, 0.4, 0.5, 0.5])
    # Gcc = g.subgraph(sorted(nx.connected_components(g), key=len, reverse=True)[0])
    # pos = nx.spring_layout(g)
    # plt.axis('off')
    # nx.draw_networkx_nodes(g, pos, node_size=20)
    # nx.draw_networkx_edges(g, pos, alpha=0.4)
    plt.show()


# test dokładności estymacji parametrów
def test_fit(n, k_tests, p_tests, deleted_edges=0, iterations=1, debug=False):
    test_results = []
    for k_next in k_tests:
        for p_next in p_tests:
            print('Testing for n = {} k = {} and p = {}'.format(n, k_next, p_next))
            C_list = []
            L_list = []
            g_base = nx.connected_watts_strogatz_graph(n, k_next, p_next)
            g_transformed = delete_edges(g_base, deleted_edges)
            C_start = nx.average_clustering(g_transformed)
            L_start = fast_average_shortest_path_length(g_transformed)
            if debug:
                print('\tC est: {:.5f}, L est {:.5f}'.format(C_start, L_start))
            for i in range(iterations):
                # generuj graf wattsa strogatza z definicji
                g_test = nx.connected_watts_strogatz_graph(n, k_next, p_next)
                # aproksymacja parametrów
                n_est, k_est, p_est = degdistrfit(g_test, debug=debug)
                if debug:
                    print('\tk est: {:.1f}, p est {:.5f}'.format(k_est, p_est))
                g_est = nx.connected_watts_strogatz_graph(n_est, k_est, p_est)
                C_est = nx.average_clustering(g_est)
                L_est = fast_average_shortest_path_length(g_est)
                if debug:
                    print('\tC est: {:.5f}, L est {:.5f}'.format(C_est, L_est))
                C_list.append(C_est)
                L_list.append(L_est)
            # wyznaczenie średnich wartości estymowanych parametrów oraz błędów estymacji
            C_avg_val = sum(C_list) / len(C_list)
            L_avg_val = sum(L_list) / len(L_list)
            K_err_list = [100 * abs(C_start - C) / C for C in C_list]
            L_err_list = [100 * abs(L_start - L) / L for L in L_list]
            C_avg_err = sum(K_err_list) / len(K_err_list)
            L_avg_err = sum(L_err_list) / len(L_err_list)
            # C_err_std = sqrt(sum((e - k_avg_err) ** 2 for e in k_err_list) / len(k_err_list))
            # L_err_std = sqrt(sum((e - p_avg_err) ** 2 for e in p_err_list) / len(p_err_list))
            test_results.append([n, k_next, p_next, C_start, L_start, C_avg_val, L_avg_val, C_avg_err, L_avg_err])
    return test_results


def read_graph(file_path):
    G = nx.read_edgelist(file_path)
    return G


def run_tests(n_list, k_list, p_list, r_edges, output_stream=sys.stdout, test_iterations=1, debug=False):
    for nodes in n_list:
        # TESTY PROGRAMU
        for r in r_edges:
            results = test_fit(n=nodes, k_tests=k_list, p_tests=p_list, deleted_edges=r, iterations=test_iterations, debug=debug)
            for t in results:
                output_stream.write('Test network with ' + str(nodes) + ' nodes and '+str(r)+' deleted edges \n')
                output_stream.write(
                    'Results for Watts-Strogatz network parameters: n = {} k = {} p = {}'
                    '\n\t C original:\t\t\t\t{:.5f}'
                    '\n\t L original:\t\t\t\t{:.5f}'
                    '\n\t C calculated:\t\t\t\t{:.5f}'
                    '\n\t L calculated:\t\t\t\t{:.5f}'
                    '\n\t avg error of C:\t\t{:.5f}'
                    ' %\n\t avg error of L:\t\t{:.5f} %'
                    # ' %\n\t std of error of k:\t\t{:.2f}'
                    # ' %\n\t std of error of p:\t\t{:.2f}%'
                    '\n'.format(*t))


def run_on_file(graph_file_path: str, debug=False):
    g = read_graph(graph_file_path)
    r = degdistrfit(g)
    print('Wattz-Strogatz estimated model parameters: n={:d}, k={:d}, p={:.3f} '.format(*r))

    # generacja grafu z wyznaczonych parametrów n,k,p
    gbis = nx.connected_watts_strogatz_graph(*r)

    # mierzenie grafu wejsciowego i wygenerowanego
    # współczynnik klasteryzacji
    g_ac = nx.average_clustering(g)
    gbis_ac = nx.average_clustering(gbis)

    # średnia długość najkrótszej ścieżki
    g_aspl = fast_average_shortest_path_length(g, debug=debug)
    gbis_aspl = fast_average_shortest_path_length(gbis, debug=debug)

    print("input graph                :"
          " average clustering coefficient                = {:9.4f} average shortest path length = {:9.4f}".format(g_ac, g_aspl))
    print("fitted Wattz-Strogatz graph:"
          " average clustering coefficient                = {:9.4f} average shortest path length = {:9.4f}".format(gbis_ac, gbis_aspl))
    print("fitted Wattz-Strogatz graph:"
          " average clustering coefficient relative error = {:9.4f}% "
		  "average shortest path length relative error = {:9.4f}%".format((gbis_ac-g_ac)/g_ac*100, (gbis_aspl - g_aspl)/g_aspl*100))
    degree_graph(gbis)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--analyze', nargs='?', help="path to file to fit", type=argparse.FileType('r'))
    parser.add_argument('-t', '--test', action='store_true')
    parser.add_argument('-n', '--n_values', nargs='*', help="list of parameter n values to test",
                        default=[10000])
    parser.add_argument('-k', '--k_values', nargs='*', help="list of parameter k values to test",
                        default=[10, 25, 50])
    parser.add_argument('-p', '--p_values', nargs='*', help="list of parameter p values to test",
                        default=[0.01, 0.1, 0.2])
    parser.add_argument('-r', '--remove_edges', nargs='*', help="number of edges to be randomly removed from WS graph",
                        default=[0, 50])
    parser.add_argument('-o', '--output', nargs='?', help="path to output file", type=argparse.FileType('w'),
                        default=sys.stdout)
    parser.add_argument('-i', '--test_iterations', default=10)
    parser.add_argument('-d', '--debug', action='store_true')

    args = parser.parse_args()
    # print(args)
    if args.test:
        args.n_values = list(set([int(i) for i in args.n_values]))
        args.k_values = list(set([int(i) for i in args.k_values]))
        args.remove_edges = list(set([int(i) for i in args.remove_edges]))
        args.p_values = list(set([float(i) for i in args.p_values]))
        args.test_iterations = int(args.test_iterations)

        print("running algorithm test with parameters:")
        print("n = {} \nk = {} \np = {}".format(args.n_values, args.k_values, args.p_values))
        run_tests(args.n_values, args.k_values, args.p_values, args.remove_edges, args.output, args.test_iterations, args.debug)
    elif args.analyze is not None:
        run_on_file(args.analyze.name, args.debug)
    else:
        print("Must run with appropriate -a (graph fitting) or -t (algorithm testing) option")
        exit(0)
if __name__ == "__main__":
    main()










