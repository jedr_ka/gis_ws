
import sys
from cx_Freeze import setup, Executable

base = 'Console'
if sys.platform == 'win32':
    base = 'Win32GUI'

options = {
    'build_exe': {
        "packages": ['scipy', 'scipy.special', 'networkx'],
        # Sometimes a little fine-tuning is needed
        # exclude all backends except wx
        'excludes': []
    }
}

executables = [
    Executable('degdistrfit.py', base=base)
]

setup(name='degdistrfit',
      version='0.1',
      description='Finding Wattz Strogatz parameters on given graph',
      executables=executables,
      options=options
      )